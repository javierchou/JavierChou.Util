﻿using System;
using System.Globalization;
using System.Reflection;
using JavierChou.Const;

namespace JavierChou
{
    public static class ObjectExtends
    {
        /// <summary> 判断对象是否为空 </summary>
        public static bool JCIsNull(this object o) => o == null || o == DBNull.Value;

        /// <summary> 判断对象是否不为空 </summary>
        public static bool JCIsNotNull(this object o) => !o.JCIsNull();

        /// <summary> 判断对象是否为空 </summary>
        public static bool JCIsNullOrEmpty(this object o) => o.JCIsNull() || o.ToString() == string.Empty;

        /// <summary> 判断对象是否不为空 </summary>
        public static bool JCIsNotNullAndEmpty(this object o) => !o.JCIsNullOrEmpty();

        /// <summary> 判断对象是否为空或零 </summary>
        public static bool JCIsNullOrEmptyOrZero(this object o) => o.JCIsNullOrEmpty() || o.ToString() == SysConst.Zero;

        /// <summary> 返回当前对象的字符串（默认为空） </summary>
        public static string JCToString(this object o) => o.JCIsNull() ? string.Empty : o.ToString();

        /// <summary> 判断对象是否为零 </summary>
        public static bool JCIsZero(this object o) => o.JCToString() == SysConst.Zero;

        /// <summary> 判断对象是否为负一（查询索引返回默认值） </summary>
        public static bool JCIsNegativeOne(this object o) => o.JCToString() == SysConst.NegativeOne;

        /// <summary> 判断对象是否不为负一 </summary>
        public static bool JCIsNotNegativeOne(this object o) => !o.JCIsNegativeOne();

        /// <summary> 返回当前对象的布尔值（默认为 false） </summary>
        public static bool JCToBool(this object o)
        {
            if (o.JCIsNullOrEmpty()) return false;

            var s = o.ToString().ToLower();
            return (!(s == SysConst.Zero || s == SysConst.False)) && (s == SysConst.One || s == SysConst.True || Convert.ToBoolean(o));
        }

        /// <summary> 返回当前对象的整型值（默认为 0） </summary>
        public static int JCToInt(this object o)
        {
            if (o.JCIsNullOrEmpty()) return 0;

            if (!int.TryParse(o.ToString(), out int result))
                ExceptionHelper.JCThrowConvertException(o.ToString(), "Int(整型)");
            return result;
        }

        /// <summary> 返回当前对象的浮点型值（默认为 0） </summary>
        public static decimal JCToDecimal(this object o)
        {
            if (o.JCIsNullOrEmpty()) return 0m;

            if (!decimal.TryParse(o.ToString(), NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out decimal result))
                ExceptionHelper.JCThrowConvertException(o.ToString(), "Decimal(浮点型)");
            return result;
        }

        /// <summary> 返回当前对象的浮点型值（默认为 0） </summary>
        public static double JCToDouble(this object o)
        {
            if (o.JCIsNullOrEmpty()) return 0.0;

            if (!double.TryParse(o.ToString(), out double result))
                ExceptionHelper.JCThrowConvertException(o.ToString(), "Double(浮点型)");
            return result;
        }

        /// <summary> 返回当前对象的长整型值（默认为 0） </summary>
        public static long JCToLong(this object o)
        {
            if (o.JCIsNullOrEmpty()) return 0L;

            if (!long.TryParse(o.ToString(), out long result))
                ExceptionHelper.JCThrowConvertException(o.ToString(), "Long(长整型)");
            return result;
        }

        /// <summary> 返回当前对象的日期时间字符形式（默认为空） </summary>
        public static string JCToDateTimeString(this object o, string format, IFormatProvider provider = null)
        {
            if (o.JCIsNullOrEmpty()) return string.Empty;

            if (!DateTime.TryParse(o.ToString(), out DateTime result))
                ExceptionHelper.JCThrowConvertException(o.ToString(), "DateTime(日期型)");
            return provider.JCIsNull() ? result.ToString(format) : result.ToString(format, provider);
        }

        /// <summary> 返回当前对象的日期字符（年-月-日）形式（默认为空） </summary>
        public static string JCToDateString(this object o) => o.JCToDateTimeString(TimeConst.DateFormatString);

        /// <summary> 返回当前对象的日期时间字符（年-月-日 时分秒）形式（默认为空） </summary>
        public static string JCToDateTimeString(this object o) => o.JCToDateTimeString(TimeConst.DateTimeFormatString);

        /// <summary> 返回当前对象的日期时间字符（年/月/日 时分秒）形式（默认为空） </summary>
        public static string JCToDateTimeString2(this object o) => o.JCToDateTimeString(TimeConst.DateTimeFormatString2, DateTimeFormatInfo.InvariantInfo);

        /// <summary> 返回当前对象的日期时间字符（年-月-日 时分）形式（默认为空） </summary>
        public static string JCToDateTimeNoSecString(this object o) => o.JCToDateTimeString(TimeConst.DateTimeFormatNoSecString);

        /// <summary> 返回当前对象的日期字符（年-月）形式（默认为空） </summary>
        public static string JCToYearMonthString(this object o) => o.JCToDateTimeString(TimeConst.YearMonthFormatString);

        /// <summary> 返回当前对象的日期字符（月-日）形式（默认为空） </summary>
        public static string JCToMonthDayString(this object o) => o.JCToDateTimeString(TimeConst.MonthDayFormatString);

        /// <summary> 返回当前对象的时间字符（时分秒）形式（默认为空） </summary>
        public static string JCToTimeString(this object o) => o.JCToDateTimeString(TimeConst.TimeFormatString);

        /// <summary> 返回当前对象的时间字符（时分）形式（默认为空） </summary>
        public static string JCToHourMinuteString(this object o) => o.JCToDateTimeString(TimeConst.HourMinuteFormatString);

        /// <summary> 返回当前对象指定的字段值 </summary>
        public static object JCGetFieldValue(this object o, string fieldName) => o.GetType().GetField(fieldName)?.GetValue(o);

        /// <summary> 将字段的值赋给当前对象 </summary>
        public static void JCSetFieldValue(this object o, string fieldName, object value) => o.GetType().GetField(fieldName).SetValue(o, value, BindingFlags.SetField, null, null);

        /// <summary> 返回当前对象指定的属性值 </summary>
        public static object JCGetPropertyValue(this object o, string property) => o.GetType().GetProperty(property)?.GetValue(o, null);

        /// <summary> 将属性的值赋给当前对象 </summary>
        public static void JCSetPropertyValue(this object o, string property, object value) => o.GetType().GetProperty(property).SetValue(o, value, null);
    }
}
