﻿using System;

namespace JavierChou
{
    public static class BoolUtils
    {
        #region >> True
        /// <summary> 判断该值为真时执行指定方法 </summary>
        public static bool JCTrue(this bool boole, Action action)
        {
            if (boole) action();
            return boole;
        }

        /// <summary> 判断该值为真时执行指定方法（一个参数） </summary>
        public static bool JCTrue<T>(this bool boole, Action<T> action, T args1)
        {
            if (boole) action(args1);
            return boole;
        }

        /// <summary> 判断该值为真时执行指定方法（二个参数） </summary>
        public static bool JCTrue<T1, T2>(this bool boole, Action<T1, T2> action, T1 args1, T2 args2)
        {
            if (boole) action(args1, args2);
            return boole;
        }

        /// <summary> 判断该值为真时执行指定方法（三个参数） </summary>
        public static bool JCTrue<T1, T2, T3>(this bool boole, Action<T1, T2, T3> action, T1 args1, T2 args2, T3 args3)
        {
            if (boole) action(args1, args2, args3);
            return boole;
        }

        /// <summary> 判断该值为真时执行指定方法并返回指定类型的值 </summary>
        public static T JCTrueOrDefault<T>(this bool boole, Func<T> func) => boole ? func() : default;
        #endregion

        #region >> False
        /// <summary> 判断该值为假时执行指定方法 </summary>
        public static bool JCFalse(this bool boole, Action action) => !JCTrue(!boole, action);

        /// <summary> 判断该值为假时执行指定方法（一个参数） </summary>
        public static bool JCFalse<T>(this bool boole, Action<T> action, T args1) => !JCTrue(!boole, action, args1);

        /// <summary> 判断该值为假时执行指定方法（二个参数） </summary>
        public static bool JCFalse<T1, T2>(this bool boole, Action<T1, T2> action, T1 args1, T2 args2) => !JCTrue(!boole, action, args1, args2);

        /// <summary> 判断该值为假时执行指定方法（三个参数） </summary>
        public static bool JCFalse<T1, T2, T3>(this bool boole, Action<T1, T2, T3> action, T1 args1, T2 args2, T3 args3) => !JCTrue(!boole, action, args1, args2, args3);

        /// <summary> 判断该值为假时执行指定方法并返回指定类型的值 </summary>
        public static T JCFalseOrDefault<T>(this bool boole, Func<T> func) => JCTrueOrDefault(!boole, func);
        #endregion

        /// <summary> 根据条件执行指定方法 </summary>
        public static void JCJudge(this bool boole, Action trueAction, Action falseAction) => (boole ? trueAction : falseAction)();
    }
}
