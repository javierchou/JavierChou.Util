﻿using JavierChou.Const;

namespace JavierChou
{
    public static class IntUtils
    {
        /// <summary> 判断对象是否为 0（一般用于比较数量） </summary>
        public static bool JCIsZero(this int i) => i == IntConst.DefaultCount;

        /// <summary> 判断对象是否大于 0（一般用于比较数量） </summary>
        public static bool JCIsGreaterThanZero(this int i) => i > IntConst.DefaultCount;

        /// <summary> 判断对象是否小于 0（一般用于比较数量） </summary>
        public static bool JCIsLessThanZero(this int i) => i < IntConst.DefaultCount;

        /// <summary> 判断对象是否为 -1（一般用于检验索引） </summary>
        public static bool JCIsNegativeOne(this int i) => i == IntConst.DefaultIndex;

        /// <summary> 判断对象是否不为 -1（一般用于检验索引） </summary>
        public static bool JCIsNotNegativeOne(this int i) => !i.JCIsNegativeOne();

        /// <summary> 将毫米转换到百分之一英寸 </summary>
        public static int JCMMToInch(this int i) => (int)(i * 100.0f / 25.4f);

        /// <summary> 将对象转换为千分位表示 </summary>
        public static string JCToThousand(this int i) => i.ToString("###,###");
    }
}
