﻿using System;
using System.Data;
using JavierChou.Const;

namespace JavierChou
{
    public static class DataSetUtils
    {
        /// <summary> 判断数据集是否为空 </summary>
        public static bool JCEmpty(this DataSet ds) => ds.JCIsNull() || ds.Tables.Count.JCIsZero();

        /// <summary> 判断数据集是否存在具有指定名称的表 </summary>
        public static bool JCContains(this DataSet ds, string tableName) => ds.Tables.Contains(tableName);

        /// <summary> 判断数据集是否不存在具有指定名称的表 </summary>
        public static bool JCNotContains(this DataSet ds, string tableName) => !ds.JCContains(tableName);

        /// <summary> 对数据集的每个元素执行指定操作 </summary>
        public static void JCForEach(this DataSet ds, Action<DataTable> action)
        {
            if (ds.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            foreach (DataTable dt in ds.Tables)
            {
                action(dt);
            }
        }
    }
}
