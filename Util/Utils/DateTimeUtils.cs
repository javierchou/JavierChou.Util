﻿using System;
using System.Globalization;
using JavierChou.Const;

namespace JavierChou
{
    public static class DateTimeUtils
    {
        /// <summary> 将指定对象值转换为时间类型（默认当前时间） </summary>
        public static DateTime JCToDateTime(this DateTime? dt) => dt.JCIsNull() ? default : Convert.ToDateTime(dt);

        /// <summary> 将指定对象值转换为 UTC 格式字符串（默认为空） </summary>
        public static string JCToUTCDateTimeString(this DateTime dt) => dt.JCIsNull() ? string.Empty : dt.ToString(TimeConst.UTCDateTimeString, CultureInfo.CreateSpecificCulture("en-US"));

        /// <summary> 返回指定对象值所在的星期 </summary>
        public static string JCDayOfWeek(this DateTime dt) => CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dt.DayOfWeek);
    }
}
