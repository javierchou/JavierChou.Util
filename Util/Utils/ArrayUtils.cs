﻿using System;
using JavierChou.Const;

namespace JavierChou
{
    public static class ArrayUtils
    {
        /// <summary> 对 T[] 的每个元素执行指定操作 </summary>
        public static void JCForEach<T>(this T[] array, Action<T> action)
        {
            if (array.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            for (int i = 0; i < array.Length; i++)
            {
                action(array[i]);
            }
        }
    }
}
