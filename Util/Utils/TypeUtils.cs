﻿using System;

namespace JavierChou
{
    public static class TypeUtils
    {
        /// <summary> 判断当前类型是否存在指定的字段名称 </summary>
        public static bool JCIsExistField(this Type type, string fieldName) => type.GetField(fieldName).JCIsNotNull();

        /// <summary> 判断当前类型是否存在指定的属性名称 </summary>
        public static bool JCIsExistProperty(this Type type, string property) => type.GetProperty(property).JCIsNotNull();
    }
}
