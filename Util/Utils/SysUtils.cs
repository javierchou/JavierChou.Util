﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace JavierChou
{
    public static class SysUtils
    {
        /// <summary> 链式操作 </summary>
        public static T2 JCNext<T1, T2>(this T1 source, Func<T1, T2> action) => action(source);

        /// <summary> 复制该对象的值（深拷贝） </summary>
        public static T JCCopy<T>(this T source) where T : class, new()
        {
            var memoryStream = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, source);
            memoryStream.Position = 0L;
            return binaryFormatter.Deserialize(memoryStream) as T;
        }

        /// <summary> 检验 </summary>
        public static void JCCheck(bool assertion, string message)
        {
            if (assertion) throw new Exception(message);
        }

        /// <summary> 创建随机码 </summary>
        public static string JCCreateRandomCode(int codeCount)
        {
            var array = new char[] { '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'X', 'Y' };
            var result = new List<char>();
            var num = -1;
            var random = new Random();
            for (int i = 0; i < codeCount; i++)
            {
                if (num != -1)
                {
                    random = new Random(i * num * (int)DateTime.Now.Ticks);
                }
                var num2 = random.Next(array.Length);
                num = num2;
                result.Add(array[num2]);
            }
            return result.JCJoin();
        }
    }
}
