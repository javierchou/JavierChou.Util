﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using JavierChou.Const;

namespace JavierChou
{
    public static class IEnumerableUtils
    {
        #region >> IEnumerable
        /// <summary> 判断集合是否包含任何元素 </summary>
        public static bool JCAny<T>(this IEnumerable<T> source) => source.JCIsNotNull() && source.Any();

        /// <summary> 判断集合是否为空 </summary>
        public static bool JCEmpty<T>(this IEnumerable<T> source) => !source.JCAny();

        /// <summary> 判断集合所有元素是否都满足条件（默认为 false） </summary>
        public static bool JCAll<T>(this IEnumerable<T> source, Func<T, bool> predicate) => source.JCAny() && source.All(predicate);

        /// <summary> 对 T 的每个元素执行指定操作 </summary>
        public static void JCForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            foreach (var item in source)
            {
                action(item);
            }
        }

        /// <summary> 将集合转化为 DataTable（默认为 null） </summary>
        public static DataTable JCToDataTable<T>(this IEnumerable<T> source, string tableName = "")
        {
            if (source.JCIsNull() || source.JCEmpty()) return null;

            var dt = new DataTable(tableName);
            source.First().GetType().GetProperties().JCForEach(x =>
            {
                if (x.PropertyType == typeof(bool) || x.PropertyType == typeof(bool?))
                    dt.Columns.JCAdd(x.Name, typeof(bool));
                else
                    dt.Columns.JCAdd(x.Name);
            });
            source.JCForEach(x =>
            {
                var dr = dt.JCNewRow();
                foreach (DataColumn dc in dt.Columns)
                {
                    dr[dc.ColumnName] = x.GetType().GetProperty(dc.ColumnName).GetValue(x);
                }
            });
            return dt;
        }

        /// <summary> 返回指定分隔符串联集合成员的字符串（默认为空） </summary>
        public static string JCJoin<T>(this IEnumerable<T> source, string separator = "") => source.JCIsNull() ? string.Empty : string.Join(separator, source);
        #endregion

        #region >> IList
        /// <summary> 将集合数据转换为字符串（按逗号分割） </summary>
        public static string JCListToString<T>(this IList<T> ls) => ls.JCAny() ? string.Join(SysConst.SplitComma, ls) : string.Empty;

        /// <summary> 将值添加到列表中 </summary>
        public static IList<T> JCAdd<T>(this IList<T> ls, T value, bool distinct = false)
        {
            if (distinct && ls.Contains(value)) return ls;

            ls.Add(value);
            return ls;
        }
        #endregion
    }
}
