﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using JavierChou.Const;
using Microsoft.JScript;

namespace JavierChou
{
    public static class StringUtils
    {
        /// <summary> 对字符串的每个元素执行指定操作 </summary>
        public static void JCForEach(this string str, Action<char> action)
        {
            if (str.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            foreach (var c in str)
            {
                action(c);
            }
        }

        /// <summary> 判断指定的字符是否出现在此字符串中 </summary>
        public static bool JCContains(this string str, char ch) => str.IndexOf(ch).JCIsNotNegativeOne();

        /// <summary> 返回指定正则表达式在指定字符串中是否找到匹配项 </summary>
        public static bool JCIsMatch(this string str, string pattern) => new Regex(pattern).IsMatch(str);

        /// <summary> 判断指定的字符是否为手机号 </summary>
        public static bool JCIsMobile(this string str) => str.JCIsMatch(SysConst.Pattern_Mobile);

        /// <summary> 判断指定的字符是否为电话 </summary>
        public static bool JCIsTel(this string str) => str.JCIsMatch(SysConst.Pattern_Tel);

        /// <summary> 判断指定的字符是否为QQ </summary>
        public static bool JCIsQQ(this string str) => str.JCIsMatch(SysConst.Pattern_QQ);

        /// <summary> 判断指定的字符是否为邮箱 </summary>
        public static bool JCIsEmail(this string str) => str.JCIsMatch(SysConst.Pattern_Email);

        /// <summary> 判断指定的字符是否为银行账户 </summary>
        public static bool JCIsBankAcount(this string str) => str.JCIsMatch(SysConst.Pattern_BankAcount);

        /// <summary> 判断指定的字符是否为网址 </summary>
        public static bool JCIsURL(this string str) => str.JCIsMatch(SysConst.Pattern_URL);

        /// <summary> 判断指定的字符是否为物理地址 </summary>
        public static bool JCIsMacAddress(this string str) => str.JCIsMatch(SysConst.Pattern_MacAddress);

        /// <summary> 判断指定的字符是否为路径 </summary>
        public static bool JCIsPath(this string str) => str.JCIsMatch(SysConst.Pattern_Path);

        /// <summary> 将指定的URL字符串进行编码 </summary>
        public static string JCUrlEncode(this string str) => str.JCIsNullOrEmpty() ? string.Empty : HttpUtility.UrlEncode(str);

        /// <summary> 将指定的URL字符串进行解码 </summary>
        public static string JCUrlDecode(this string str) => str.JCIsNullOrEmpty() ? string.Empty : HttpUtility.UrlDecode(str);

        /// <summary> 将指定的URL字符串进行编码 </summary>
        public static string JCEncodeURIComponent(this string str) => str.JCIsNullOrEmpty() ? string.Empty : GlobalObject.encodeURIComponent(str);

        /// <summary> 将指定的URL字符串进行解码 </summary>
        public static string JCDecodeURIComponent(this string str) => str.JCIsNullOrEmpty() ? string.Empty : GlobalObject.decodeURIComponent(str);

        /// <summary> 从当前字符串对象移除所有前后空白字符（默认为空） </summary>
        public static string JCTrim(this string str) => str.JCIsNull() ? string.Empty : str.Trim();

        /// <summary> 返回此字符串拆分后的指定索引位置的字符串（默认为 null） </summary>
        public static string JCSplit(this string str, char splitChar, int index)
        {
            var array = str.Split(new char[] { splitChar });
            return array.Length > 0 ? array[index] : null;
        }

        /// <summary> 返回此字符串拆分后的第一个字符串（默认为 null） </summary>
        public static string JCFirstSplit(this string str, char splitChar) => str.JCSplit(splitChar, 0);

        /// <summary> 返回此字符串拆分后的最后一个字符串（默认为 null） </summary>
        public static string JCLastSplit(this string str, char splitChar)
        {
            var array = str.Split(new char[] { splitChar });
            return array.Length > 0 ? array[array.Length - 1] : null;
        }

        /// <summary> 返回此字符串的小写形式（默认为空） </summary>
        public static string JCToLower(this string str) => str.JCToString().ToLower();

        /// <summary> 返回此字符串的大写形式（默认为空） </summary>
        public static string JCToUpper(this string str) => str.JCToString().ToUpper();

        /// <summary> 返回此字符串的 In 形式（默认为空） </summary>
        public static string JCToInString(this string str) => str.JCIsNullOrEmpty() ? string.Empty : string.Format("{0}{1}{0}", SysConst.SplitSingQuotMark, str.Replace(SysConst.SplitComma, "','"));

        /// <summary> 返回此字符串按顺序截取指定个数的新字符串（默认为空） </summary>
        public static string JCTake(this string str, int count) => str.JCIsNullOrEmpty() ? string.Empty : str.Length > count ? str.Substring(0, count) : str;

        /// <summary> 返回此字符串从指定索引位置开始到末尾的新字符串（默认为空） </summary>
        public static string JCSkip(this string str, int index) => str.JCIsNullOrEmpty() || str.Length < index ? string.Empty : str.Substring(index);

        /// <summary> 返回此字符串移除最后一个字符的新字符串（默认为空） </summary>
        public static string JCRemoveLastChar(this string str) => str.JCTake(str.Length - 1);

        /// <summary> 返回此字符串移除末尾指定字符串的新字符串 </summary>
        public static string JCRemoveLastString(this string str, string removeString) => str.EndsWith(removeString) ? str.Substring(0, str.Length - removeString.Length) : str;

        /// <summary> 返回此字符串合并后的新字符串 </summary>
        public static string JCMergeIDString(this string str, string otherIds) => str.Split(SysConst.SplitCharComma).Union(otherIds.Split(SysConst.SplitCharComma)).JCJoin(SysConst.SplitComma);

        /// <summary> 返回此字符串去重后的新字符串（以逗号分割） </summary>
        public static string JCToDistinctString(this string str) => str.JCIsNullOrEmpty() ? str : str.Split(SysConst.SplitCharComma).Distinct().JCJoin(SysConst.SplitComma);
    }
}
