﻿using System;
using System.Collections.Generic;
using System.Data;
using JavierChou.Const;

namespace JavierChou
{
    public static class DataTableUtils
    {
        #region >> DataTable
        /// <summary> 判断表是否包含任何元素 </summary>
        public static bool JCAny(this DataTable dt) => dt.JCIsNotNull() && dt.Rows.Count.JCIsGreaterThanZero();

        /// <summary> 判断表是否为空 </summary>
        public static bool JCEmpty(this DataTable dt) => !dt.JCAny();

        /// <summary> 创建与该表具有相同架构的新 DataRow，并添加到该表 </summary>
        public static DataRow JCNewRow(this DataTable dt)
        {
            if (dt.JCIsNull()) return null;

            var dr = dt.NewRow();
            dt.Rows.Add(dr);
            return dr;
        }

        /// <summary> 对表的每个元素执行指定操作 </summary>
        public static void JCForEach(this DataTable dt, Action<DataRow> action)
        {
            if (dt.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            foreach (DataRow dr in dt.Rows)
            {
                action(dr);
            }
        }

        /// <summary> 返回数据表中指定列的所有数据 </summary>
        public static List<T> JCGetCloumnValue<T>(this DataTable dt, string columnName)
        {
            var result = new List<T>();
            if (dt.JCIsNull() || dt.Columns.JCNotContains(columnName)) return result;

            foreach (DataRow dr in dt.Rows)
            {
                result.JCAdd((T)dr[columnName], true);
            }
            return result;
        }
        #endregion

        #region >> DataRow
        /// <summary> 返回当前数据行中指定列数据（若为空则返回默认值） </summary>
        public static T JCGetValueOrDefault<T>(this DataRow dr, string columnName, Func<Type, T> defaultor = null)
        {
            if (dr.JCIsNull() || dr.Table.Columns.JCNotContains(columnName)) return default;

            var value = dr[columnName];
            if (value.JCIsNullOrEmpty() && defaultor.JCIsNotNull())
                value = defaultor(dr.Table.Columns[columnName].DataType);
            return (T)value;
        }

        /// <summary> 返回与该 DataRow 相同架构和数据的新 DataRow </summary>
        public static DataRow JCCopy(this DataRow dr)
        {
            if (dr.JCIsNull()) return null;

            var newRow = dr.Table.NewRow();
            newRow.ItemArray = dr.ItemArray;
            return newRow;
        }

        /// <summary> 将该 DataRow 数据复制到指定的 DataRow（相同列名） </summary>
        public static void JCCopyTo(this DataRow dr, DataRow destRow, bool ignoreDataType = true)
        {
            if (dr.JCIsNull() || destRow.JCIsNull()) return;

            foreach (DataColumn dc in destRow.Table.Columns)
            {
                if (dr.Table.Columns.JCNotContains(dc.ColumnName)) continue;
                if (!ignoreDataType && dc.DataType != dr.Table.Columns[dc.ColumnName].DataType) continue;

                destRow[dc.ColumnName] = dr[dc.ColumnName];
            }
        }
        #endregion

        #region >> DataColumnCollection
        /// <summary> 判断集合是否不包含具有指定名称的列 </summary>
        public static bool JCNotContains(this DataColumnCollection dcc, string columnName) => !dcc.Contains(columnName);

        /// <summary> 对列集的每个元素执行指定操作 </summary>
        public static void JCForEach(this DataColumnCollection dcc, Action<DataColumn> action)
        {
            if (dcc.JCIsNull()) return;

            if (action.JCIsNull())
                ExceptionHelper.JCThrowArgumentNullException(SysConst.DefaultAction);
            foreach (DataColumn dc in dcc)
            {
                action(dc);
            }
        }

        /// <summary> 创建指定列并添加到集合 </summary>
        public static DataColumn JCAdd(this DataColumnCollection dcc, string columnName)
        {
            if (dcc.JCNotContains(columnName))
                dcc.Add(new DataColumn(columnName));
            return dcc[columnName];
        }

        /// <summary> 创建指定列并添加到集合 </summary>
        public static DataColumn JCAdd(this DataColumnCollection dcc, string columnName, Type type)
        {
            if (dcc.JCNotContains(columnName))
                dcc.Add(new DataColumn(columnName, type));
            return dcc[columnName];
        }

        /// <summary> 创建指定列并添加到集合 </summary>
        public static DataColumn JCAdd(this DataColumnCollection dcc, string columnName, Type type, bool allowDBNull)
        {
            if (dcc.JCNotContains(columnName))
                dcc.Add(new DataColumn(columnName, type) { AllowDBNull = allowDBNull });
            return dcc[columnName];
        }
        #endregion
    }
}
