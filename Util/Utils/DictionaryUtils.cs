﻿using System.Collections.Generic;

namespace JavierChou
{
    public static class DictionaryUtils
    {
        /// <summary> 返回此字典中与指定键关联的值 </summary>
        public static TValue JCGetValue<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key) => dic.ContainsKey(key) ? dic[key] : default;

        /// <summary> 将指定键和值添加到字典中 </summary>
        public static Dictionary<TKey, TValue> JCAdd<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            if (key.JCIsNotNullAndEmpty())
            {
                if (dic.ContainsKey(key))
                    dic[key] = value;
                else
                    dic.Add(key, value);
            }
            return dic;
        }

        /// <summary> 将指定键和值添加到字典中 </summary>
        public static Dictionary<T, List<K>> JCAdd<T, K>(this Dictionary<T, List<K>> dic, T key, K value, bool distinct = false)
        {
            if (dic.JCNotContainsKey(key))
                dic[key] = new List<K>();
            dic[key].JCAdd(value, distinct);
            return dic;
        }

        /// <summary> 判断字典是否不包含指定键 </summary>
        public static bool JCNotContainsKey<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key) => !dic.ContainsKey(key);
    }
}
