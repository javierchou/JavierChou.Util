﻿namespace JavierChou.Readonly
{
    internal static class StringReadonly
    {
        /// <summary> 数字（中文） </summary>
        public static readonly string[] NumberChinese = new string[]
        {
            "零",
            "一",
            "二",
            "三",
            "四",
            "五",
            "六",
            "七",
            "八",
            "九"
        };

        /// <summary> 危险字符（数据库关键字） </summary>
        public static readonly string[] DangerousCharacter = new string[]
        {
            "exec",
            "insert",
            "delete",
            "update",
            "add",
            "master",
            "truncate",
            "drop",
            "create",
            "grant",
            "deny",
            "revoke",
            "prepare",
            "execute"
        };

        /// <summary> 文件扩展名（Excel） </summary>
        public static readonly string[] FileExtnsion_Excel = new string[]
        {
            "xls",
            "xlsx"
        };

        /// <summary> 文件扩展名（PPT） </summary>
        public static readonly string[] FileExtnsion_PPT = new string[]
        {
            "ppt",
            "pptx"
        };

        /// <summary> 文件扩展名（文本） </summary>
        public static readonly string[] FileExtnsion_Doc = new string[]
        {
            "txt",
            "doc",
            "docx"
        };

        /// <summary> 文件扩展名（图片） </summary>
        public static readonly string[] FileExtnsion_Image = new string[]
        {
            "jpg",
            "jpeg",
            "gif",
            "bmp",
            "png"
        };

        /// <summary> 文件扩展名（TIF） </summary>
        public static readonly string[] FileExtnsion_Tif = new string[]
        {
            "tif",
            "tiff"
        };

        /// <summary> 文件扩展名（PDF） </summary>
        public static readonly string[] FileExtnsion_PDF = new string[]
        {
            "pdf"
        };

        /// <summary> 文件扩展名（音频） </summary>
        public static readonly string[] FileExtnsion_Music = new string[]
        {
            "mp3",
            "wav"
        };

        /// <summary> 文件扩展名（视频） </summary>
        public static readonly string[] FileExtnsion_Video = new string[]
        {
            "mp4",
            "mov",
            "wmv",
            "rm"
        };
    }
}
