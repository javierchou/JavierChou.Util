﻿using System;

namespace JavierChou
{
    internal static class ExceptionHelper
    {
        /// <summary> 参数为空异常 </summary>
        public static void JCThrowArgumentNullException(string argumentName) => throw new Exception($"The argument({argumentName}) is null.");

        /// <summary> 转换失败异常 </summary>
        public static void JCThrowConvertException(string value, string destValue) => throw new Exception($"The {value} isn't convert to {destValue}.");
    }
}
