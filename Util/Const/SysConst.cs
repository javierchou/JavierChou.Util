﻿namespace JavierChou.Const
{
    internal static class SysConst
    {
        /// <summary> 项目简称 </summary>
        public const string ProjectAbbreviation = "JC";
        /// <summary> 项目全称 </summary>
        public const string ProjectFullName = "JavierChou";

        /// <summary> 默认参数名称（action） </summary>
        public const string DefaultAction = "action";
        /// <summary> 默认 </summary>
        public const string Default = "default";
        /// <summary> 网页空格 </summary>
        public const string HtmlSpace = "&nbsp;";

        /// <summary> 空 </summary>
        public const string Null = "null";
        /// <summary> 未定义 </summary>
        public const string Undefined = "undefined";

        /// <summary> 负一 </summary>
        public const string NegativeOne = "-1";
        /// <summary> 零 </summary>
        public const string Zero = "0";
        /// <summary> 一 </summary>
        public const string One = "1";

        /// <summary> 是 </summary>
        public const string True = "true";
        /// <summary> 否 </summary>
        public const string False = "false";

        /// <summary> 正则表达式：手机 </summary>
        public const string Pattern_Mobile = "^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$";
        /// <summary> 正则表达式：电话 </summary>
        public const string Pattern_Tel = "^(\\d{3,4}-)?\\d{6,8}$";
        /// <summary> 正则表达式：QQ </summary>
        public const string Pattern_QQ = "^[1-9][0-9]{4,}$";
        /// <summary> 正则表达式：邮箱 </summary>
        public const string Pattern_Email = "^\\w+([\\._-]\\w+)*@\\w+([-\\.]\\w+)*\\.\\w+([-\\.]\\w+)*$";
        /// <summary> 正则表达式：银行账户 </summary>
        public const string Pattern_BankAcount = "^\\d{12,20}$";
        /// <summary> 正则表达式：网址 </summary>
        public const string Pattern_URL = "^((https|http|ftp|rtsp|mms)?:\\/\\/)[^\\s]+";
        /// <summary> 正则表达式：物理地址 </summary>
        public const string Pattern_MacAddress = "^([0-9a-fA-F]{2})(([/\\s:-][0-9a-fA-F]{2}){5})$";
        /// <summary> 正则表达式：路径 </summary>
        public const string Pattern_Path = "^[a-zA-Z]:(((\\\\(?! )[^/:*?<>\\\"|\\\\]+)+\\\\?)|(\\\\)?)\\s*$";

        /// <summary> 分隔符：单引号 </summary>
        public const string SplitSingQuotMark = "'";
        /// <summary> 分隔符：逗号 </summary>
        public const string SplitComma = ",";
        /// <summary> 分隔符：点 </summary>
        public const string SplitDot = ".";

        /// <summary> 逗号分隔符 </summary>
        public const char SplitCharComma = ',';
        /// <summary> 点分隔符 </summary>
        public const char SplitCharDot = '.';
    }
}
