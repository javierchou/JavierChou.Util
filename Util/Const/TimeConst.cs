﻿namespace JavierChou.Const
{
    internal static class TimeConst
    {
        /// <summary> 最小日期 </summary>
        public const string MinDate = "1900-01-01 ";
        /// <summary> 最小年 </summary>
        public const string MinYear = "1900";
        /// <summary> 最小日 </summary>
        public const string MindDay = "01";

        /// <summary> 格式化 年-月-日 </summary>
        public const string DateFormatString = "yyyy-MM-dd";
        /// <summary> 格式化 年-月 </summary>
        public const string YearMonthFormatString = "yyyy-MM";
        /// <summary> 格式化 年 </summary>
        public const string YearFormatString = "yyyy";
        /// <summary> 格式化 月-日 </summary>
        public const string MonthDayFormatString = "MM-dd";

        /// <summary> 格式化 年-月-日 时分秒 </summary>
        public const string DateTimeFormatString = "yyyy-MM-dd HH:mm:ss";
        /// <summary> 格式化 年-月-日 时分 </summary>
        public const string DateTimeFormatNoSecString = "yyyy-MM-dd HH:mm";
        /// <summary> 格式化 年-月-日 时分秒.毫秒 </summary>
        public const string DateTimeFullFormatString = "yyyy-MM-dd HH:mm:ss.fff";
        /// <summary> 格式化 时分秒 </summary>
        public const string TimeFormatString = "HH:mm:ss";
        /// <summary> 格式化 时分 </summary>
        public const string HourMinuteFormatString = "HH:mm";
        /// <summary> 格式化 时分秒.毫秒 </summary>
        public const string TimeFullFormatString = "HH:mm:ss.fff";

        /// <summary> 格式化 年/月/日 时分秒 </summary>
        public const string DateTimeFormatString2 = "yyyy/MM/dd HH:mm:ss";

        /// <summary> 格式化 UTC </summary>
        public const string UTCDateTimeString = "ddd MMM d HH:mm:ss 'UTC'zz'00' yyyy";
    }
}
