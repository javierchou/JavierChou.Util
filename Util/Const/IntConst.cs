﻿namespace JavierChou.Const
{
    internal static class IntConst
    {
        /// <summary> 默认值 数量 </summary>
        public const int DefaultCount = 0;
        /// <summary> 默认值 索引 </summary>
        public const int DefaultIndex = -1;
    }
}
