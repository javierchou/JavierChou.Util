﻿using System;
using System.Collections.Generic;
using JavierChou;
using Test.Samples;

namespace Test
{
    internal class Program
    {
        static void Main(string[] args) => Demo.Builder().AddAction(TestAction()).Test();

        static IEnumerable<Action> TestAction() => new List<Action>()
            .JCAdd(new CommonSample().TestLinkCall)
            .JCAdd(new ConvertSample().TestMMToInch)
            .JCAdd(new ConvertSample().TestToThousand)
            .JCAdd(new BoolSample().TestTrueAndFalse);
    }
}
