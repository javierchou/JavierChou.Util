﻿using System;

namespace Test
{
    internal class ConsoleHelper
    {
        public static void Write(string msg) => Console.Write(msg);

        public static void WriteLine(string msg, bool hasPrefix = true) => Console.WriteLine((hasPrefix ? ">>" : "") + msg);

        public static int Read() => Console.Read();

        public static string ReadLine() => Console.ReadLine();
    }
}
