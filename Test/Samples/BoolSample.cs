﻿using JavierChou;

namespace Test.Samples
{
    internal class BoolSample
    {
        private void TrueAction() => ConsoleHelper.WriteLine(true.ToString());

        private void FalseAction() => ConsoleHelper.WriteLine(false.ToString());

        public void TestTrueAndFalse()
        {
            true.JCTrue(TrueAction).JCFalse(FalseAction);
            false.JCTrue(TrueAction).JCFalse(FalseAction);
        }
    }
}
