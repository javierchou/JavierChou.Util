﻿using System;
using JavierChou;

namespace Test.Samples
{
    internal class CommonSample
    {
        public void TestLinkCall() => ConsoleHelper.WriteLine(DateTime.Now.JCNext(x => "链式调用：" + x).JCNext(x => x + " ").JCNext(x => x + DateTime.Now.JCDayOfWeek()));
    }
}
