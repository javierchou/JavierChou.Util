﻿using JavierChou;

namespace Test.Samples
{
    internal class ConvertSample
    {
        public void TestMMToInch() => ConsoleHelper.WriteLine("将毫米【230】转换到百分之一英寸：" + 230.JCMMToInch());

        public void TestToThousand() => ConsoleHelper.WriteLine("将数值【1234567890】转换为千分位表示：" + 1234567890.JCToThousand());
    }
}
