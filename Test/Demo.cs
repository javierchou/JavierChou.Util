﻿using System;
using System.Collections;
using System.Collections.Generic;
using JavierChou;

namespace Test
{
    internal class Demo
    {
        private Queue _actions;

        private Demo() { _actions = new Queue(); }

        public static Demo Builder() => new Demo();

        public Demo AddAction(Action action)
        {
            if (action.JCIsNotNull())
                this._actions.Enqueue(action);
            return this;
        }

        public Demo AddAction(IEnumerable<Action> action)
        {
            action.JCForEach(x => AddAction(x));
            return this;
        }

        public void Test()
        {
            ConsoleHelper.WriteLine("------测试（开始）------\n", false);
            while (this._actions.Count > 0)
            {
                ((Action)this._actions.Dequeue()).Invoke();
            }
            ConsoleHelper.WriteLine("\n------测试（结束）------", false);
            ConsoleHelper.Write("请按任意键退出...");
            ConsoleHelper.Read();
        }
    }
}
